#!/usr/bin/env node

//const pretty = require('pretty');
var fs = require("fs");
const raml2html = require('raml2html');
const yargs = require('yargs');

const argv = yargs
  .usage('Usage: raml2readme -i [input_file]')
  .version('1.2.2')

  .alias('v', 'version')

  .help('h')
  .alias('h', 'help')

  .string('i')
  .alias('i', 'input')
  .describe('i', 'Input file')

  .boolean('d')
  .alias('d', 'document')
  .describe('d', 'Only use the documnetation portion')

  .string('o')
  .alias('o', 'output')
  .describe('o', 'Output file - default README.md')
  .argv;

let input = argv.input;
let output = argv.output;
let doc = argv.document;

if(!input){
  if(argv._.length !==1){
    console.error('Error: You need to specify the RAML input file\n');
    yargs.showHelp();
    process.exit(1);
  }
}

if(!output){
  output = "README.md";
}


fs.readFile(input, 'utf8', function (err,data) {
  if (err) { return console.log(err); }

  var outFile ="";

  //header
  if(doc){
    var api = data.match(/(\ntitle:\s+[a-zA-Z0-9]+)/g)+"";
    api = api.substr(8,api.length) + " documentation,";

    var version = (data.match(/(\nversion:\s+[a-zA-Z0-9]+)/g)+"").replace("\n"," ");

    outFile += "## "+api + version +"\n\n---\n\n"
  }

  //remove all before documentation tagvar reg = /(\\n[a-zA-Z0-9])/gm
  var result = data.substr(data.indexOf("documentation")+15,data.length);
  if(result.substr(0,result.indexOf("\n")).includes("!include")){
    result = includes(input,result,"doc");
  }
  //remove everything after the next root node
  result = result.substr(0, result.search(/(\n[a-zA-Z0-9])/gm));


  while( result.includes("content")){

      var title = result.substr(result.indexOf("title:")+6,result.indexOf("\n")-6);
      result = result.substr(result.indexOf("\n")-10,result.length);

      outFile += "###"+title;

      var content = "";

      if( result.includes("title:")){
        content = result.substr(result.indexOf("content:")+9,result.indexOf("title:")-28);
        result = result.substr(result.indexOf("title:"), result.length);
      }
      else{
        //end of titles
        content = result.substr(result.indexOf("content:")+9,result.length);
        result = "";
      }
      if(content.includes("!include")){
        //included another file
        content = "\n" + includes(input,content,"content");

      }
      else{
        //removes extra space from the RAML
        content = content.replace(/(    [^\s])/g,
                        function getLastChat(str){return str.charAt(str.length-1);} );
      }
      outFile += content.replace("|",'').replace(/( [\s]```)/g, "```")+'\n\n';
  }

  if(doc){
      fs.writeFile(output, outFile, 'utf8', function (err) {
        if (err) console.log(err);
        console.log("Successfully written " + input + " to " + output + ".");
      });
  }
  else{
    const config = raml2html.getConfigForTemplate(__dirname + '/index.nunjucks');

    var endLine = new RegExp(escapeRegExp("$ENDLINE "), 'g');
    var tab = new RegExp(escapeRegExp("$TAB"), 'g');
    var documentation = new RegExp(escapeRegExp("$DOCUMENTATION"), 'g');


    raml2html.render(input, config, "p").then(result => {
      result = result.replace(endLine, "\r\n")
                      .replace(tab,"    ")
                      .replace(documentation, outFile);

      fs.writeFile(output,result, (err) => {
        if (err) console.log(err);
        console.log("Successfully written " + input + " to " + output + ".");
      });

    }, error => { console.log(error);} );
  }
});



function includes(input, result, type){
  var include = fs.readFileSync(input.substr(0,input.lastIndexOf("/"))
        +"/"+ result.substr(result.indexOf("!include")+9,result.indexOf("\n")-9)).toString('utf-8');
  if(type == "doc"){
    include = verifySpacing(include);
  }
  else{
    include = verifySpacingContent(include);
  }
  return include + result.substr(result.indexOf("\n"),result.length);
}

//checks spacing from document include
function verifySpacing(str){
  if(str.search(/(\n[^\s])/g)>0){
    return "  " + str.replace(/(\n)/g,"\n  ");
  }
  else{
    return str;
  }
}

//checks spacing of content include
function verifySpacingContent(str){
  if(str.search(/(\n[^\s])/g)>0){
    var string = "  " + str.replace(/(\n)/g,"\n  ")
    return string;
  }
  else{
    return str;
  }
}

function escapeRegExp(str) {
    return str.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1");
}
