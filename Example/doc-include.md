## ExpensesAPI documentation, version: v1

---

### Welcome
   
  This is an example README nested inside of the example raml file
  It is quite simple but it does prove a point. The next sections
  reuse the readme of this project.


### Summary
  The purpose of this node package is to auto generate an API's README file using the raml associated with the API.
  raml2html does already have its own markdown theme but it is kind of poor.


### Usage
  - You can either install the package with npm by using 'npm install -g raml2readme'
  - Or you can clone the package manually
      - *Requires:*
          - raml2html
          - yargs
          - fs
  - `Use` `raml2readme -h` for all the other information you need
  - `raml2readme -i <<file>>` will use a default file of README.md as the output
  - `raml2readme -i <<file>> -o <<output>>` will ouput the content to the given output file
  - You can add additional documentation to your readme using the `documentation` tag in your raml. You can see this in `Example/OUTPUT.md`


### Test
  This is a code block
```json
  {
    "Test": "Test"
  }
```
  


