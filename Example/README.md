# ExpensesAPI documentation, version v1 
http://localhost:8080/api 

### Welcome
   
  This is an example README nested inside of the example raml file
  It is quite simple but it does prove a point. The next sections
  reuse the readme of this project.


### Summary
  The purpose of this node package is to auto generate an API's README file using the raml associated with the API.
  raml2html does already have its own markdown theme but it is kind of poor.


### Usage
  - You can either install the package with npm by using 'npm install -g raml2readme'
  - Or you can clone the package manually
      - *Requires:*
          - raml2html
          - yargs
          - fs
  - `Use` `raml2readme -h` for all the other information you need
  - `raml2readme -i <<file>>` will use a default file of README.md as the output
  - `raml2readme -i <<file>> -o <<output>>` will ouput the content to the given output file
  - You can add additional documentation to your readme using the `documentation` tag in your raml. You can see this in `Example/OUTPUT.md`


### Test
  This is a code block
```json
  {
    "Test": "Test"
  }
```



 ---

### /masterExpense 
- **GET**: Get the sum of each expense category by month for the given year. 
    - **Query Parameters:**
        - *year: number* 
            - *Display Name: Year*
            - *Description: masterExpenses with the given Year.*
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
    - **Responses:** *[ 200 ,400 ]*

### /masterExpense/expenses 
- **GET**: Gets every expense currently listed in the database. 
    - **Query Parameters:**
        - *category?: string* 
            - *Display Name: Category*
            - *Description: Expense with the given Category.*
        - *day?: number* 
            - *Display Name: Day*
            - *Description: Expenses with the given Day.*
        - *gt?: number* 
            - *Display Name: Greater Than*
            - *Description: Ammounts Greater than.*
        - *lt?: number* 
            - *Display Name: Less Than*
            - *Description: Ammounts Less than.*
        - *month?: string* 
            - *Display Name: Month*
            - *Description: expenses with the given Month.*
        - *year?: number* 
            - *Display Name: Year*
            - *Description: expenses with the given Year.*
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
        - *account?: number* 
            - *Display Name: Account*
            - *Description: Account number that paid for expense*
    - **Responses:** *[ 200 ]*
- **POST** *(secured)*: Creates a new expense. 
    - **Responses:** *[ 201 ,400 ,500 ]*

### /masterExpense/expenses/bulk 
- **POST**: Create multiple expenses at once. 
    - **Responses:** *[ 201 ,400 ,500 ]*

### /masterExpense/expenses/{id} 
- **GET**: Get the expense with the given id. 
    - **Query Parameters:**
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
    - **Responses:** *[ 200 ,404 ]*
- **PATCH**: Update the expense with the given id. 
    - **Responses:** *[ 200 ,400 ,404 ]*
- **DELETE**: Remove the expense with the given id. 
    - **Responses:** *[ 204 ,404 ,500 ]*


### /masterIncome 
- **GET**: Get the sum of each income account by month for the given year. 
    - **Query Parameters:**
        - *year: number* 
            - *Display Name: Year*
            - *Description: masterIncomes with the given Year.*
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
    - **Responses:** *[ 200 ,400 ]*

### /masterIncome/incomes 
- **GET**: Gets every income currently listed in the database. 
    - **Query Parameters:**
        - *day?: number* 
            - *Display Name: Day*
            - *Description: Expenses with the given Day.*
        - *gt?: number* 
            - *Display Name: Greater Than*
            - *Description: Ammounts Greater than.*
        - *lt?: number* 
            - *Display Name: Less Than*
            - *Description: Ammounts Less than.*
        - *month?: string* 
            - *Display Name: Month*
            - *Description: incomes with the given Month.*
        - *year?: number* 
            - *Display Name: Year*
            - *Description: incomes with the given Year.*
        - *account?: number* 
            - *Display Name: Account*
            - *Description: Account number that paid for expense*
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
    - **Responses:** *[ 200 ]*
- **POST** *(secured)*: Create a new Income. 
    - **Responses:** *[ 201 ,400 ,500 ]*

### /masterIncome/incomes/bulk 
- **POST**: Create multiple expenses at once. 
    - **Responses:** *[ 201 ,400 ,500 ]*

### /masterIncome/incomes/{id} 
- **GET**: Get the income with the given id. 
    - **Query Parameters:**
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
    - **Responses:** *[ 200 ,404 ]*
- **PATCH**: Update the income with the given id. 
    - **Responses:** *[ 200 ,400 ,404 ]*
- **DELETE**: Remove the income with the given id. 
    - **Responses:** *[ 204 ,404 ,500 ]*


### /accounting 
- **GET**: Get accounting T book for given year and month. 
    - **Headers:**
        - *Authorization: string* 
            - *Display Name: Authorization*
            - *Description: Basic authentication base 64 encoded string*
        - *Tenant-Id: string* 
            - *Display Name: Tenant-Id*
            - *Description: Tenant id for multi-tenant environments*
        - *Content-type: string* 
            - *Display Name: Content-type*
            - *Description: either xml or json*
    - **Query Parameters:**
        - *month: string* 
            - *Display Name: Month*
            - *Description: accountings with the given Month.*
        - *year: number* 
            - *Display Name: Year*
            - *Description: accountings with the given Year.*
        - *account?: number* 
            - *Display Name: Account*
            - *Description: Account number that paid for expense*
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
    - **Responses:** *[ 200 ,400 ]*

### /accounting/paystubs 
- **GET**: Get all pay stub data. 
    - **Query Parameters:**
        - *year: number* 
            - *Display Name: Year*
            - *Description: paystubs with the given Year.*
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
    - **Responses:** *[ 200 ]*
- **POST**: Create a new paystub. 
    - **Query Parameters:**
        - *createIncome?: boolean* 
            - *Display Name: Create Income*
            - *Description: Create income when creating paystub.*
        - *account?: number* 
            - *Display Name: Account*
            - *Description: Account number that paid for expense*
    - **Responses:** *[ 201 ,400 ]*

### /accounting/paystubs/{id} 
- **GET**: Get paystub with given ID. 
    - **Query Parameters:**
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
    - **Responses:** *[ 200 ,404 ]*
- **PATCH**: Update paystub with given ID. 
    - **Responses:** *[ 200 ,400 ,404 ]*
- **DELETE**: Remove paystub with given ID. 
    - **Responses:** *[ 204 ,404 ,500 ]*

### /accounting/accounts 
- **GET**: Get a list of all accounts. 
    - **Responses:** *[ 200 ]*
- **POST**: Create a new account. 
    - **Responses:** *[ 201 ,400 ]*

### /accounting/accounts/{id} 
- **GET**: Get a single account with the given ID. 
    - **Responses:** *[ 200 ,404 ]*
- **PATCH**: Update the account with the given ID. 
    - **Responses:** *[ 200 ,400 ,404 ]*
- **DELETE**: Delete an account from the database. 
    - **Responses:** *[ 204 ,404 ,500 ]*

### /accounting/loans 
- **GET**: Get a list of all accounts. 
    - **Query Parameters:**
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
    - **Responses:** *[ 200 ]*
- **POST**: Create a new Loan. 
    - **Responses:** *[ 201 ,400 ]*

### /accounting/loans/{id} 
- **GET**: Get a single loan with the given ID. 
    - **Query Parameters:**
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
    - **Responses:** *[ 200 ,404 ]*
- **PATCH**: Update the loan with the given ID. 
    - **Responses:** *[ 200 ,400 ,404 ]*
- **DELETE**: Delete a loan from the database. 
    - **Responses:** *[ 204 ,404 ,500 ]*

### /accounting/loans/{id}/loanPayment 
- **GET**: Get a list of all loan paymants. 
    - **Query Parameters:**
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
    - **Responses:** *[ 200 ]*
- **POST**: Add a loan payment to the loan with the given ID. 
    - **Responses:** *[ 201 ,400 ]*

### /accounting/loans/{id}/loanPayment/{id} 
- **GET**: Get a single loan payment with the given ID. 
    - **Query Parameters:**
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
    - **Responses:** *[ 200 ]*
- **PATCH**: Update the loan payment with the given ID. 
    - **Responses:** *[ 200 ,400 ,404 ]*
- **DELETE**: Delete a loan payment from the database. 
    - **Responses:** *[ 204 ,404 ,500 ]*

### /accounting/loans/{id}/estimate 
- **GET**: Get a payment plan for the given loan. 
    - **Query Parameters:**
        - *html?: boolean* 
            - *Display Name: HTML*
            - *Description: Enables HTML output when true.*
        - *payment?: number* 
            - *Display Name: Payment*
            - *Description: Default payment ammount to use*
    - **Responses:** *[ 200 ,400 ,404 ]*

### End Raml